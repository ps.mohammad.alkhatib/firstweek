package processor;

public class Operator {

   public OperatorEnum operator;
   private int priority;


   public Operator(OperatorEnum operator){

       this.operator = operator;
       int priority = -1;
       switch (this.operator){
           case multiply -> priority = 10;
           case divide -> priority = 8;
           case plus -> priority = 6;
           case minus -> priority = 4;
       }
       this.priority = priority;
   }

   private int priority (){
       return this.priority;
   }

   public int compareTo (Operator another){
       return this.priority() - another.priority();
   }

   public String priorityOver (Operator another){
       if (this.compareTo(another) == 0)
           return "equal";
       else if (this.compareTo(another) > 0)
           return "higher";

       return "lower";
   }
}
