package processor;

public class Main {

    private static OperatorsFactory factory;

    public static void main(String[] args) {

//        Main.factory = OperatorsFactory.getFactory();
//
//        testOperatorClass();
//        EquationProcessor.solve("31+5*6-7");
//        String s = "1a23";
//        System.out.println(Double.valueOf(s));
        EquationProcessor.solve("31+5*6-7");
    }

    public static void testOperatorClass(){

        Operator op1 = factory.getMultiplicationOperator();

        Operator op2 = factory.getDivisionOperator();

        Operator op3 = factory.getAdditionOperator();

        Operator op4 = factory.getSubtractionOperator();
        System.out.println(op1.priorityOver(op2));
        System.out.println(op2.priorityOver(op1));
        System.out.println(op2.priorityOver(op4));
        System.out.println(op3.priorityOver(op4));
    }
}