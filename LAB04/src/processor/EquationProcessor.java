package processor;

import java.util.ArrayList;
import java.util.Stack;

public class EquationProcessor {

    private static OperatorsFactory factory = OperatorsFactory.getFactory();

    private String equation;
    private double result;

    private Stack <Operator>  operatorsStack;

    private Stack <Double> operandsStack;


    public static double solve (String str){
        ArrayList <String> items = resolve (str);
//        for (int i = 0; i < items.size(); i++) {
//            System.out.println(items.get(i));
//        }

        return result (items);
    }

    public static double result (ArrayList <String> arrList){
        Stack<Double> numbers = new Stack<>();
        Stack<Operator> operators = new Stack<>();
        for (String s : arrList) {
            if (isNumber(s)){

                double d = Double.parseDouble(s);

                numbers.push(d);
            }
            else{

                Operator op = operatorObject(s);

                operators.push(op);
            }
        }

        print(numbers);
        print(operators);
        return 1.0;
    }

    private static  Operator operatorObject(String s) {

        return switch (s.charAt(0)) {
            case '+' ->  factory.getAdditionOperator();//  Operator(OperatorEnum.plus);
            case '-' -> factory.getSubtractionOperator();
            case '*' -> factory.getMultiplicationOperator();
            case '/' -> factory.getDivisionOperator();
            default -> null;
        };
    }


    public static void print (Stack  s){

        while (!s.empty()){
            System.out.println(s.pop());
        }
    }


    private static boolean isNumber(String str){

        if (str.length()>1)
            return true;
        return str.charAt(0) > 47 && str.charAt(0) <58;
    }



    public static ArrayList <String> resolve (String str) {
        StringBuilder stringBuilder = new StringBuilder();
        ArrayList <String> temp = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i)> 47 && str.charAt(i) <58) {
                stringBuilder.append(str.charAt(i));
            } else {
                temp.add(stringBuilder.toString());
                temp.add(String.valueOf(str.charAt(i)));
                stringBuilder.delete(0,i);
            }

        }
        temp.add(stringBuilder.toString());
        return temp;

    }
}
