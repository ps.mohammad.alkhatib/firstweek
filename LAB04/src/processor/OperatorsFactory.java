package processor;

public class OperatorsFactory {

    private static OperatorsFactory factory;

    private OperatorsFactory(){}


    public  static OperatorsFactory getFactory(){
        if (factory == null) {
            factory = new OperatorsFactory();
        }
        return factory;
    }

    public Operator getAdditionOperator(){
        return new Operator(OperatorEnum.plus);
    }

    public Operator getSubtractionOperator(){
        return new Operator(OperatorEnum.minus);
    }

    public Operator getMultiplicationOperator(){
        return new Operator(OperatorEnum.multiply);
    }

    public Operator getDivisionOperator(){
        return new Operator(OperatorEnum.divide);
    }
}
