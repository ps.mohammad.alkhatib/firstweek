package lab1;

public class Matrix {

    private final int[][] values;

    public Matrix(int[][] values) {
        this(values, true);
    }
                                                                   
    private Matrix(int[][] values, boolean external) {
        if(external) {
            if (values == null)
                throw new IllegalArgumentException("Array is null!");
            if (!isMatrix(values))
                throw new IllegalArgumentException("Array is not valid Matrix");
            this.values = this.deepCopy(values);
            return;
        }
        this.values = values;
    }


    private boolean isMatrix(int[][] arr) {
        if (arr[0] == null) return false;
        int firstRowLength = arr[0].length;
        for (int[] ints : arr) {
            if (ints == null) return false;
            if (ints.length != firstRowLength) return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int[] value : values) {
            sb.append("[");
            for (int v : value) {
                sb.append(" ").append(v).append(" ");
            }
            sb.append("]");
            sb.append("\n");
        }
        return sb.toString();
    }


    // TODO this is the same as cols, remove the usage of it


    public int rowsNum() {
        return values.length;
    }


    public int columnsNum() {
        return values[0].length;
    }

    // TODO this is not overriding for equals
    // TODO override equals and hashcode (read about it)
    public boolean sameDimension(Matrix another) {
        return sameRowsCount(another) && sameColsCount(another);
    }

    private boolean sameColsCount(Matrix another) {
        return values[0].length == another.values[0].length;
    }

    private boolean sameRowsCount(Matrix another) {
        return values.length == another.values.length;
    }

    public boolean isSquare() {
        return values[0].length == values.length;
    }

    private int calcDeterminant(Matrix matrix){
        int sum = 0;
        int multiplier = 1;

        for (int i = 0; i < matrix.columnsNum(); i++) {
            sum = sum + multiplier * matrix.values[0][i] *
                    determinant(matrix.subMatrix(0,i));
            multiplier *= -1;
        }
        return sum;
    }

    private int determinant(Matrix matrix){
        if (matrix.rowsNum() == 2) {
            return matrix.values[0][0] *
                    matrix.values[1][1] - matrix.values[0][1]
                    * matrix.values[1][0];
        }

        return calcDeterminant(matrix);
    }

    public Matrix subMatrix(int rowNum, int colNum){
        int[][] result = new int[this.rowsNum() - 1][this.columnsNum() - 1];
        int row = 0;
        for (int i = 0; i < this.rowsNum(); i++) {
            int column = 0;
            if (i == rowNum)
                continue;
            for (int j = 0; j < this.columnsNum(); j++) {
                if (j == colNum)
                    continue;
                result[row][column++] = this.values[i][j];
            }
            row++;
        }
        return new Matrix(result, false);
    }

    public int determinant(){
        return determinant(this);
    }

    private int[][] deepCopy(int [][] arr){
        int [][] temp = new int[arr.length][arr[0].length];
        for (int i = 0; i < arr.length; i++) {
            System.arraycopy(arr[i], 0, temp[i], 0, arr[0].length);
        }
        return temp;
    }


    //+++++++++++++++++++++++Moved from Multi Dimensional array+++++++++++++++++++++++++++++++
    public  Matrix sum(Matrix right) {

        if (!this.sameDimension(right))
            throw new IllegalArgumentException("they dont have same size");

        int[][] result = new int[this.rowsNum()][this.columnsNum()];
        for (int i = 0; i < this.rowsNum(); i++) {
            for (int j = 0; j < this.columnsNum(); j++) {
                result[i][j] = this.values[i][j] + right.values[i][j];
            }
        }

        return new Matrix(result,false);
    }

    public  Matrix scale(int number) {

        int[][] result = new int[this.rowsNum()][this.columnsNum()];
        for (int i = 0; i < this.rowsNum(); i++) {
            for (int j = 0; j < this.columnsNum(); j++) {
                result[i][j] = this.values[i][j] * number;
            }
        }
        return new Matrix(result,false);
    }

    public  Matrix transport() {

        int[][] result = new int[this.columnsNum()][this.rowsNum()];

        for (int i = 0; i < this.rowsNum(); i++) {
            for (int j = 0; j < this.columnsNum(); j++) {
                result[j][i] = this.values[i][j];
            }

        }
        return new Matrix(result,false);
    }

    public Matrix multiply(Matrix right) {

        // check if left column equal right rows.
        if (this.columnsNum() != right.rowsNum())
            throw new IllegalArgumentException
                    ("left column should be equal right row");

        int[][] result = new int[this.rowsNum()][right.columnsNum()];
        for (int i = 0; i < this.rowsNum(); i++) {
            for (int j = 0; j < right.columnsNum(); j++) {
                for (int k = 0; k < this.columnsNum(); k++) {
                    result[i][j] += this.values[i][k] * right.values[k][j];
                }
            }
        }
        return new Matrix(result,false);
    }

    public  Matrix toDiagonal() {
        //check if square
        if (!this.isSquare())
            throw new IllegalArgumentException("should be square!");
        int[][] result = this.copy();
        for (int i = 0; i < this.rowsNum(); i++) {
            for (int j = 0; j < this.columnsNum(); j++) {
                if (i != j)
                    result[i][j] = 0;
            }

        }
        return new Matrix(result,false);
    }
    private int[][] copy() {
        int[][] copyArray = new int[this.rowsNum()][this.columnsNum()];
        for (int i = 0; i < this.rowsNum(); i++) {
            if (this.columnsNum() >= 0)
                System.arraycopy(this.values[i], 0, copyArray[i],
                        0, this.columnsNum());
        }
        return copyArray;
    }

    public  Matrix toLowerTriangle() {

        int[][] result = this.copy();
        for (int i = 0; i < this.rowsNum(); i++) {
            for (int j = 0; j < this.rowsNum(); j++) {
                if (j > i)
                    result[i][j] = 0;
            }
        }
        return new Matrix(result,false);
    }
    public  Matrix toUpperTriangle() {

        int[][] result = this.copy();
        for (int i = 0; i < this.rowsNum(); i++) {
            for (int j = 0; j < this.columnsNum(); j++) {
                if (j < i)
                    result[i][j] = 0;
            }
        }
        return new Matrix(result,false);
    }

}
