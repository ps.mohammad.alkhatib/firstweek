package lab1;

public class Test {

    public static void main(String[] args) {

        testSum();
        testScale();
        testTransport();
        testMultiply();

        testToDiagonal();
        testToLowerTriangle();
        testTOUpperTriangle();

        testSubMatrix();
        testDeterminant();

    }



    private static void testSum() {

        int[][] a = {{1,3,1},{1,0,0}};
        int[][] a2 = {{0,0,5},{7,5,0}};
        Matrix left = new Matrix(a);
        Matrix right = new Matrix(a2);

        System.out.println("_______________");
        //Matrix sum = MultiDimensionalArrays.sum(new Matrix(a),new Matrix(a2));

        System.out.println(left.sum(right));
    }
    private static void testScale() {

        int[][] b = {{1,8,-3},{4,-2,5}};
        Matrix bMatrix = new Matrix(b);
        System.out.println("_______________");
      //  Matrix scale = MultiDimensionalArrays.scale(new Matrix(b),2);
        System.out.println(bMatrix.scale(2).toString());
    }

    private static void testTransport(){
        int[][] c = {{1,2,3},{0,-6,7}};
        Matrix cMatrix = new Matrix(c);
        System.out.println("_______________");
        //Matrix transportedMatrix = MultiDimensionalArrays.transport(new Matrix(c));
        System.out.println(cMatrix.transport().toString());

    }

    private static void testMultiply(){
        int[][] e1 = {{1,2,3},{4,5,6}};
        int[][] e2 = {{7,8},{9,10},{11,12}};
        Matrix e1Matrix = new Matrix(e1);
        Matrix e2Matrix = new Matrix(e2);
        //Matrix result = MultiDimensionalArrays.multiply(new Matrix(e1),new Matrix(e2));
        System.out.println(e1Matrix.multiply(e2Matrix).toString());
    }


    private static void testToDiagonal(){
        int[][]  f  = {{1,2,3},{4,5,6},{7,8,9}};
        Matrix fMatrix = new Matrix(f);
        System.out.println("_______________");
        //Matrix result = MultiDimensionalArrays.toDiagonal(new Matrix(f));
        System.out.println(fMatrix.toDiagonal());
    }


    private static void testToLowerTriangle(){

        int[][]  f  = {{1,2,3},{4,5,6},{7,8,9}};
        Matrix fMatrix = new Matrix(f);

        System.out.println("_______________");
      //  Matrix result = MultiDimensionalArrays.toLowerTriangle(new Matrix(f));
        System.out.println(fMatrix.toLowerTriangle());
    }

    private static void testTOUpperTriangle(){
        int[][]  f  = {{1,2,3},{4,5,6},{7,8,9}};
        Matrix fMatrix = new Matrix(f);

        System.out.println("_______________");
      //  Matrix result = MultiDimensionalArrays.toUpperTriangle(new Matrix(f));
        System.out.println(fMatrix.toUpperTriangle());
    }

    private static void testSubMatrix(){
        int[][] f2 = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        System.out.println("_______________");
        Matrix f2Matrixx = new Matrix(f2);

       // Matrix result = MultiDimensionalArrays.subMatrix(new Matrix(f2),2,1);
        System.out.println(f2Matrixx.subMatrix(2,1));
    }

    private static void testDeterminant(){
        int[][] arr = {{8, 5, 4,4, 67},
                {687, 68, 974, 546, 5},
                {65, 65, 12, 56, 34},
                {1, 23, 35,6,7},
                {32, 123, 23,32,32}};
        System.out.println("_______________");
        int result = new Matrix(arr).determinant();
        System.out.println(result);

    }
}