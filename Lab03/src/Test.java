import bst.BinaryTree;

public class Test {
    public static void main(String[] args) {

        testBT();
    }

    public static void testBT(){
        BinaryTree bt = new BinaryTree();
        System.out.println(bt.treeDepth());//-1
        bt.accept(3);
        bt.accept(2);
        bt.accept(0);
        bt.accept(5);
        bt.accept(7);
        bt.accept(10);
        bt.accept(1);
        System.out.println(bt.depth(7));//3
        System.out.println(bt.accept(10));//false
        bt.accept(11);
        bt.accept(8);
        System.out.println(bt.treeDepth());//5

    }
}