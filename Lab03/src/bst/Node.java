package bst;

import java.util.function.Consumer;

public class Node {

    private final int value;
    private Node right;
    private Node left;


    public Node(int value) {
        this.value = value;
    }


    public boolean accept(int value) {
        if (value == this.value)
            return false;
        if (value > this.value) {
            return accept(value, right, this::assignAsRight);
        }
        return accept(value, left, this::assignAsLeft);
    }

    private boolean accept(int value, Node node, Consumer<Node> assigner) {
        if (node == null) {
            node = new Node(value);
            assigner.accept(node);
            return true;
        }
        return node.accept(value);
    }


    public int depth(int element) {
        if (this.value == element)
            return 1;
        if (element > this.value)
            return findInLeaf(element, this.right);
        return findInLeaf(element, this.left);
    }

    private static int findInLeaf(int element, Node leaf) {
        if (leaf == null)
            return -1;
        int depth = leaf.depth(element);
        return depth == -1 ? -1 : depth + 1;
    }


    private void assignAsRight(Node n) {
        right = n;
    }

    private void assignAsLeft(Node n) {
        left = n;
    }

    public int treeDepth(Node node){
        int leftDepth = dive(node.left)+1;
        int rightDepth = dive(node.right)+1;
        return Math.max(leftDepth, rightDepth);
    }

    public int dive (Node node){
        if (node == null)
            return 0;
        return treeDepth(node);
    }


}



