package bst;

import java.util.NoSuchElementException;

public class BinaryTree {
    private Node root;
    private BTState state = new BinaryTree.EmptyBT();// ask about

    private interface BTState {

        public boolean accept(int value);
    }
    private class EmptyBT implements BinaryTree.BTState {
        public boolean accept(int value){
            BinaryTree.this.setRoot(new Node(value));
            BinaryTree.this.changeState(new BinaryTree.NotEmptyBT());
            return true;
        }
    }

    private class NotEmptyBT implements BinaryTree.BTState{
        public boolean accept(int value){
            return BinaryTree.this.root.accept(value);
        }
    }

    public boolean accept(int value) {
        // TODO read about state design pattern
        return state.accept(value);
    }


    private void changeState(BTState btState) {
        state = btState;
    }

    public int depth(int element) {
        if (isEmpty())
            return -1;
        return root.depth(element);
    }

    public int treeDepth(){
        if (isEmpty())
            return -1;
        return root.treeDepth(this.root);//ask about
    }

    public boolean isEmpty() {
        return root == null;
    }

    private void setRoot(Node node){
        this.root = node;
    }
}
