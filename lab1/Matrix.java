package lab1;

import java.util.Arrays;
import java.util.Collections;

public class Matrix {

    private int[][] arr;
    private final int columnsNum;
    private final int rowsNum;


    public Matrix (int [][] arr){
        if (arr==null)
            throw new IllegalArgumentException("Array is null!");
        if (!isMatrix(arr))
            throw new IllegalArgumentException("Array is not valid Matrix");
        this.arr= Arrays.copyOf(arr,arr.length);
        this.rowsNum = arr.length;
        this.columnsNum = arr[0].length;
    }



    private boolean isMatrix(int [][] arr){
        if (arr[0] == null)
            return false;
        int firstRowLength = arr[0].length;
        for (int[] ints : arr) {
            if (ints == null) return false;
            if (ints.length != firstRowLength)
                return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < rowsNum; i++) {
            stringBuilder.append("[");
            for (int j = 0; j < columnsNum; j++) {
                stringBuilder.append(" "+ arr[i][j]+ " ");
            }
            stringBuilder.append("]");
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public int [] row(int i){
        return arr[i];
    }

    public int rowsNum(){
        return rowsNum;
    }

    public int columnsNum(){
        return columnsNum;
    }
    public boolean equals(Matrix another){
        return this.rowsNum == another.rowsNum && this.columnsNum == another.columnsNum;
    }

    public int element(int i, int j){
        return arr[i][j];
    }

    public boolean isSquare(){
        return columnsNum == rowsNum;
    }
}
