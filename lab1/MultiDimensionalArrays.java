package lab1;

public class MultiDimensionalArrays {
    // TODO you should check that the matrix is a valid one or not

    public static Matrix sum(Matrix m1, Matrix m2) {

        if (!isSameSize(m1, m2))
            throw new IllegalArgumentException("they dont have same size");

        int[][] result = new int[m1.rowsNum()][m1.columnsNum()];
        for (int i = 0; i < m1.rowsNum(); i++) {
            for (int j = 0; j < m1.columnsNum(); j++) {
                result[i][j] = m1.element(i,j) + m2.element(i,j);
            }
        }

        return new Matrix(result);
    }

    public static Matrix scale(Matrix matrix, int number) {

        int[][] result = new int[matrix.rowsNum()][matrix.columnsNum()];
        for (int i = 0; i < matrix.rowsNum(); i++) {
            for (int j = 0; j < matrix.columnsNum(); j++) {
                result[i][j] = matrix.element(i,j) * number;
            }
        }
        return new Matrix(result);
    }

    public static Matrix transport(Matrix matrix) {

        int[][] result = new int[matrix.columnsNum()][matrix.rowsNum()];

        for (int i = 0; i < matrix.rowsNum(); i++) {
            for (int j = 0; j < matrix.columnsNum(); j++) {
                result[j][i] = matrix.element(i,j);
            }

        }
        return new Matrix(result);
    }


    public static Matrix multiply(Matrix m1, Matrix m2) {

        // check if left column equal right rows.
        if (m1.columnsNum() != m2.rowsNum())
            throw new IllegalArgumentException
                    ("left column should be equal right row");

        int[][] result = new int[m1.rowsNum()][m2.columnsNum()];
        for (int i = 0; i < m1.rowsNum(); i++) {
            for (int j = 0; j < m2.columnsNum(); j++) {
                for (int k = 0; k < m1.row(i).length; k++) {
                    result[i][j] += (m1.element(i,k) * m2.element(k,j));
                }
            }
        }
        return new Matrix(result);
    }

    public static Matrix toDiagonal(Matrix matrix) {

        //check if square
        if (!matrix.isSquare())
            throw new IllegalArgumentException("should be square!");


        int[][] result = copy(matrix);
        for (int i = 0; i < matrix.rowsNum(); i++) {
            for (int j = 0; j < matrix.columnsNum(); j++) {
                if (i != j)
                    result[i][j] = 0;
            }

        }
        return new Matrix(result);
    }

    public static Matrix toLowerTriangle(Matrix matrix) {

        int[][] result = copy(matrix);
        for (int i = 0; i < matrix.rowsNum(); i++) {
            for (int j = 0; j < matrix.rowsNum(); j++) {
                if (j > i)
                    result[i][j] = 0;
            }
        }
        return new Matrix(result);
    }


    public static Matrix toUpperTriangle(Matrix matrix) {

        int[][] result = copy(matrix);
        for (int i = 0; i < matrix.rowsNum(); i++) {
            for (int j = 0; j < matrix.columnsNum(); j++) {
                if (j < i)
                    result[i][j] = 0;
            }
        }
        return new Matrix(result);
    }


    public static Matrix subMatrix(Matrix matrix, int rowNum, int colNum) {


        int[][] result = new int[matrix.rowsNum()-1][matrix.columnsNum()-1];
        int row = 0;
        for (int i = 0; i < matrix.rowsNum(); i++) {
            int column = 0;
            if (i == rowNum)
                continue;
            for (int j = 0; j < matrix.columnsNum(); j++) {
                if (j == colNum)
                    continue;
                result[row][column++] = matrix.element(i,j);
            }
            row++;
        }
        return new Matrix(result);
    }

    private static int[][] copy(Matrix matrix) {


        int[][] copyArray = new int[matrix.rowsNum()][matrix.columnsNum()];
        for (int i = 0; i < matrix.rowsNum(); i++) {
            for (int j = 0; j < matrix.columnsNum(); j++) {
                copyArray[i][j] = matrix.element(i,j);
            }

        }
        return copyArray;
    }


    public static void toString(int[][] array) {
        if (array == null)
            throw new IllegalArgumentException("array is null!");

        for (int i = 0; i < array.length; i++) {
            System.out.print("[");
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(" " + array[i][j] + " ");
            }
            System.out.print("]");
            System.out.println("");
        }

    }

    public static boolean isSameSize(Matrix m1, Matrix m2) {
        // TODO we need to check if the array is actually a matrix
        return m1.equals(m2);
    }




    public static int determinant(Matrix matrix) {

        if (matrix.rowsNum()==2) {
            return matrix.element(0,0) * matrix.element(1,1) - matrix.element(0,1)
                    * matrix.element(1,0);
        }

        return calculateDeterminant(matrix);
    }

    private static int calculateDeterminant(Matrix matrix) {
        int sum = 0;
        int multiplier = 1;

        for (int i = 0; i < matrix.columnsNum(); i++) {
            sum = sum + multiplier * matrix.element(0,i) *
                    determinant(subMatrix(matrix, 0, i));
            multiplier *= -1;
        }
        return sum;
    }


}
