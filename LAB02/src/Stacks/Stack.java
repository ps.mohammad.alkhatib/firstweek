package Stacks;


public interface Stack {

    public void push(String str);

    default void push(String... value) {
        for (String v : value) {
            push(v);
        }
    }
    public String pop();
    public int size();
    public String peek();
    public void print();

}
