package Stacks;
import java.util.NoSuchElementException;

public class DynamicArrayStack implements Stack {
    private String [] arr;
    private int n;

    private Boolean fixedSize;

    public DynamicArrayStack(){
        arr = new String[2];
        n = 0;
    }

    public void push(String str){
        if (str == null)
            throw new IllegalArgumentException("Argument is null!");

        if (size() == arr.length)
            resize(arr.length * 2);

        arr[n++] = str;

    }
    public String pop(){
        if (isEmpty())
            throw new NoSuchElementException("Stack is Empty");
        String temp = arr[n-1];
        arr[n-1] = null;
        n--;
        if(n > 4 && n == arr.length/4)
            resize(arr.length/2);

        return temp;
    }
    public int size(){
        return n;
    }

    public String peek(){
        if (isEmpty()) {
            System.out.println("Stack is Empty");
            return null;
        }
        else
            return arr[n-1];
    }

    private boolean isEmpty(){
        return n==0;
    }

    public void print(){
        for (int i = n-1; i >= 0; i--) System.out.println(arr[i]);
    }

    private void resize(int newCapacity){

        String [] temp = new String[newCapacity];
        for (int i = 0; i < n; i++) {
            temp[i] = arr[i];
        }
        arr = temp;
    }
}
