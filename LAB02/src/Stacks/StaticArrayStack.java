package Stacks;

import java.util.NoSuchElementException;

public class StaticArrayStack implements Stack {
    // TODO duplicate code

    private final String[] values;
    private int n;


    public StaticArrayStack() {
        this(4);
    }

    public StaticArrayStack(int size) {
        values = new String[size];
        n = 0;
    }

    public void push(String str) {
        if (str == null)
            throw new IllegalArgumentException("Argument is null!");
        if (size() == values.length)
            throw new IllegalStateException("Stack is Full!");
        values[n++] = str;
    }

    public String pop() {
        if (isEmpty())
            throw new NoSuchElementException("Stack is Empty");
        String temp = values[n - 1];
        values[n--] = null;
        return temp;
    }

    public int size() {
        return n;
    }

    public String peek() {
        if (isEmpty()) {
            return null;// TODO as in pop
        }
        return values[n - 1];
    }

    private boolean isEmpty() {
        return n == 0;
    }

    public void print() {
        for (int i = n - 1; i >= 0; i--)
            System.out.println(values[i]);
    }
}
