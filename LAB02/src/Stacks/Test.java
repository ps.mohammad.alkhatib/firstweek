package Stacks;

import Stacks.*;
import Queues.*;



public class Test {
    public static void main(String[] args) {
          //testStaticStack();
         //testDynamicStack();
        // testStaticQueue();
       // testDynamicQueue();


    }







    public static void testStaticStack(){
       // Stack stack1 = new StaticArrayStack(3);
        //System.out.println(stack1.pop());
//        stack1.push("mo");
//        stack1.push("bilal");
//        stack1.push("alkhatib");
//       // stack1.push("test if out of size");
//        stack1.print();
//        System.out.println("__________________________");
//        System.out.println("peek:"+ stack1.peek());
//        System.out.println(("poped: " + stack1.pop()));
//        System.out.println("poped: "+ stack1.pop());
//        System.out.println("__________________________");
//        stack1.print();
    }

    public static void testDynamicStack(){
//        Stack stack1 = new DynamicArrayStack();
//        stack1.push("moD");
//        stack1.push("bilalD");
//        stack1.push("alkhatibD");
//        stack1.push("111");
//        stack1.push("222");
//        stack1.push("333");
//        stack1.print();
//        System.out.println("__________________________");
//        System.out.println("peek:"+ stack1.peek());
//        System.out.println(("poped: " + stack1.pop()));
//        System.out.println("poped: "+ stack1.pop());
//        System.out.println("__________________________");
//        stack1.print();
    }

    public static void testStaticQueue(){
        Queue que = new StaticArrayQueue(5);
        //System.out.println(que.dequeue()); queue is empty
        que.enqueue("00");
        que.enqueue("11");
        que.enqueue("22");
        que.enqueue("33");
        que.enqueue("44");
        //que.enqueue("55"); queue is full
        que.print();
        System.out.println("dequed: "+ que.dequeue());
        System.out.println("_____");
        que.print();
        System.out.println("size: " +que.size());
        System.out.println("dequed: "+ que.dequeue());
        System.out.println("dequed: "+ que.dequeue());
        que.print();

    }
    public static void testDynamicQueue(){
        Queue que = new DynamicArrayQueue();
      //   System.out.println(que.dequeue()); queue is empty
        que.enqueue("00");
        que.enqueue("11");
        que.enqueue("22");
        que.enqueue("33");
        que.enqueue("44");
        que.enqueue("55");
        que.enqueue("66");
        que.print();
        System.out.println("dequed: "+ que.dequeue());
        System.out.println("_____");
        que.print();
        System.out.println("size: " +que.size());
        System.out.println("dequed: "+ que.dequeue());
        System.out.println("dequed: "+ que.dequeue());
        que.print();

    }
}