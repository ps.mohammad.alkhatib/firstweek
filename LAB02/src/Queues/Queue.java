package Queues;

public interface Queue {

    public void enqueue(String str);
    public String dequeue();
    public String peek();
    public int size();
    public void print();

}
