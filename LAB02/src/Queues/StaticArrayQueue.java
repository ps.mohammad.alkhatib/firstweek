package Queues;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class StaticArrayQueue implements Queue{

    private int n;
    private int head;
    private int tail;
    private String [] arr;

    public StaticArrayQueue(){
         arr = new String[4];

    }
    public StaticArrayQueue(int size){
        arr = new String [size];
    }


    public void enqueue(String str){
        if (str == null)
            throw new IllegalArgumentException("Argument is null");
        if (tail == arr.length)
            throw new IllegalStateException("Queue is full");

        arr[tail] = str;
        tail++;
        n++;

    }
    public String dequeue(){
        if(isEmpty())
            throw new NoSuchElementException("Queue is Empty");
        String temp = arr[head];
        arr[head] = null;

        n--;
        arr = shiftToLeft(arr);
       return temp;
    }
    public String peek(){
        return arr[head];
    }
    public int size(){
        return n;
    }
    public void print(){

        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    private boolean isEmpty(){
        return n==0;
    }
    private String [] shiftToLeft(String [] arr){
         String [] temp = Arrays.copyOf(arr,arr.length);

        for (int i = 0; i < size(); i++) {
            temp[i] = arr[i+1];
        }
        temp[size()]=null;
        return temp;
    }
}
