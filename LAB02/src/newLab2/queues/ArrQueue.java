package newLab2.queues;

import java.util.NoSuchElementException;

public class ArrQueue {

    private int numOfElement;
    private int head;
    private int tail;
    private String [] values;
    private boolean fixed;

    private ArrQueue(int size, boolean fixed){
        values = new String[size];
        head = -1;
        tail = -1;
        this.fixed = fixed;
    }

    public static ArrQueue dynamicQueue(int initialSize){
       return new ArrQueue(initialSize, false);
    }
    public static ArrQueue dynamicQueue(){
        return new ArrQueue(2,false);
    }
    public static ArrQueue staticQueue(int size){
        return new ArrQueue(size, true);
    }

    public void enqueue(String str){

        if (numOfElement == values.length) {
            if (fixed)
                throw new IllegalStateException("Queue is full");
            resize(size() *2);
        }

        tail++;
        tail = tail % values.length;

        values[tail]= str;

        if (numOfElement == 0)
            head = tail;

        numOfElement++;
    }

    public String dequeue(){
        if(isEmpty())
            throw new NoSuchElementException("Queue is Empty");


        String temp = values[head];
        values[head] = null;
        head++;
        head = head % values.length;
        numOfElement--;
        return temp;
    }
    public String peek(){
        if (isEmpty())
            throw new NoSuchElementException("Queue is Empty");
        return values[head];
    }



    public boolean isEmpty(){
        return numOfElement == 0;
    }
    public int size(){
        return values.length;
    }
    private void resize(int newCapacity){

        String [] temp = new String[newCapacity];
        for (int i = 0; i < values.length; i++) {
            temp[i] = values[i];
        }
        values = temp;
    }

}
