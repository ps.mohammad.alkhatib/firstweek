package newLab2.stacks;

import java.util.NoSuchElementException;

public class ArrStack {

    private String[] values;
    private int n;
    private final boolean fixed;



    private ArrStack(int size, boolean fixed){
        this.values = new String[size];
        this.fixed = fixed;
    }

    public static ArrStack staticStack(int size){
        return new ArrStack(size, true);
    }

    public static ArrStack dynamicStack(int initialSize){
        return new ArrStack(initialSize, false);
    }

    public static ArrStack dynamicStack(){
        return new ArrStack(4, false);
    }
    public void push(String str) {

        if (size() == values.length) {
            if (fixed)
                throw new IllegalStateException("Stack is full");
            resize(size()*2);
        }
        values[n++] = str;
    }

    public int size(){
        return n;
    }


    public String pop() {
        if (isEmpty())
            throw new NoSuchElementException("Stack is Empty");
        String temp = values[n - 1];

        //values[n--] = null;
          values[n-1] = null;
          n--;
        if(n > 4 && n == values.length/4 && !fixed)
            resize(values.length/2);

        return temp;
    }

    public String peek(){
        if (isEmpty())
            throw new NoSuchElementException("Stack is Empty");
        return values[n-1];
    }

    private void resize(int newCapacity){

        String [] temp = new String[newCapacity];
        for (int i = 0; i < n; i++) {
            temp[i] = values[i];
        }
        values = temp;
    }

    public boolean isEmpty(){
        return n==0;
    }
    public void print() {
        for (int i = n - 1; i >= 0; i--)
            System.out.println(values[i]);

        System.out.println("______");
    }

}
