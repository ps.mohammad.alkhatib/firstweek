package newLab2;

import newLab2.queues.ArrQueue;
import newLab2.stacks.*;


public class test {

    public static void main(String[] args) {
//        testStaticStack();
//        testDynamicStack();
        //testDynamicQueue();
        testStaticQueue();
    }

    public static void testStaticStack(){
        ArrStack stack = ArrStack.staticStack(5);
        stack.push("1");
        stack.push("2");
        stack.print();//2 1
        stack.pop();//2
        stack.print();//1

        stack.push("a");
        stack.push("b");
        stack.push("c");
        stack.push("d");
        stack.print();//d c b a 1
        //stack.push("e");// is full
    }

    public static void testDynamicStack(){
        ArrStack stack = ArrStack.dynamicStack(2);
        stack.push("1");
        stack.push("2");
        stack.push("3");
        stack.push("4");
        stack.print();// 4 3 2 1
        System.out.println(stack.peek()); //4
        System.out.println(stack.pop());//4
        System.out.println(stack.peek());//3
        System.out.println("__");
        stack.print();// 3 2 1
    }


    public static void testStaticQueue(){
        ArrQueue queue = ArrQueue.staticQueue(4);

        queue.enqueue("1");
        queue.enqueue("2");
        queue.enqueue("3");
        System.out.println(queue.dequeue());//1
        System.out.println(queue.peek());//2
        queue.enqueue("4");
        queue.enqueue("5");
        System.out.println(queue.dequeue());

    }

    public static void testDynamicQueue(){
        ArrQueue queue = ArrQueue.dynamicQueue(1);
        queue.enqueue("a");
        queue.enqueue("b");
        queue.enqueue("c");
        queue.enqueue("d");
        System.out.println(queue.dequeue());//a
        System.out.println(queue.dequeue());//b

        queue.dequeue();
        System.out.println(queue.peek());//d
    }
}
